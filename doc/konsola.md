# Organizacja przestrzeni roboczych ROS

## Tworzenie przestrzeni roboczej ROS

0. Sprawdź czy jest załadowana główna przestrzeń robocza ROS-a:

    ```
    roscd
    ```

    powinno przenieść nas do katalogu `/opt/ros/noetic`

1. Przejdź do katalogu domowego użytkownika:

    ```
    cd
    ```

2. Stwórz katalogi własnej przestrzeni roboczej

    ```
    mkdir -p sipr_ws/src
    ```

    ![mkdir](figures/mkdir.png)

3. Wejdź do katalogu `~/sipr_ws/src` i zainicjuj przestrzeń roboczą

    ```
    cd sipr_ws/src
    ```

    ```
    catkin_init_workspace
    ```

4. Wejdź do katalogu `~/sipr_ws` (jeden poziom wyżej) i zbuduj przestrzeń roboczą

    ```
    cd ..
    ```

    ```
    catkin_make
    ```

5. W każdej nowej sesji terminala należy załadować naszą przestrzeń roboczą poleceniem:

    ```
    source ~/sipr_ws/devel/setup.bash
    ```

    Aby nie musieć powtarzać ładowania można dodać tę instrukcję do pliku `~/.bashrc`, który jest uruchamiany przy otwarciu nowej sesji terminala.

    ```
    echo "source ~/sipr_ws/devel/setup.bash" >> ~/.bashrc
    ```

6. Teraz `roscd` przenosić nas będzie do katalogu `~/sipr_ws/devel/`

    ```
    roscd
    ```

    ![roscd_2](figures/roscd_2.png)

## Poruszanie się w przestrzeniach roboczych ROS-a (`roscd`)

Po wykonaniu poprzedniego punktu ćwiczenia mamy stworzoną przestrzeń roboczą `sipr_ws` w katalogu domowym użytkownika. Przestrzeń ta jest domyślnie załadowana w każdej sesji konsoli, dlatego poniższe polecenie przeniesie nas do katalogu `sipr_ws/devel`. Analogicznie jak polecenie `cd`, które przenosi nas do katalogu domowego Linuxa. 

```
roscd
```

Przejdźmy poziom wyżej, do głównego katalogu przestrzeni roboczej `sipr_ws`. 

```
cd ..
```

Wyświetlmy układ katalogów w przestrzeni roboczej: 

```
tree ~/sipr_ws -L 1
```

![Drzewo katalogów](figures/workspace_tree.png)

* `src` - zawiera pliki źródłowe (kod C++ i inne pliki tekstowe) tworzonego oprogramowania
* `build` - zawiera pliki generowane podczas kompilowania kodu
* `devel` - zawiera pliki wykonywalne oraz inne "produkty końcowe" procesu kompilacji

Omawiany katalog zawiera tę część oprogramowania dostępną w systemie ROS, którą rozwijamy samodzielnie. Pozostałe programy znajdują się w katalogu instalacyjnym (czyli typowo `/opt/ros/noetic`).

Poruszając się po katalogach z wykorzystaniem narzędzia `roscd` nie musimy wiedzieć, w jakim katalogu znajduje się dana pakiet. Jeśli chcemy np. przejść w terminalu do pakietu `roscpp`, to wpisujemy komendę:

```
roscd roscpp
```

Na przykładzie tego polecenia możemy też zobaczyć, jak działa w ROSie auto-uzupełnianie. Jeśli wpisując nazwę pakietu wciśniemy klawisz `TAB`:

```
roscd roscpp_tut<<< teraz wciśnij klawisz TAB >>>
```

to nazwa pakietu zostanie automatycznie dokończona:

```
roscd roscpp_tutorials/
```

Jeśli kilka pakietów ma identyczny wpisany przez nas początek nazwy, to uzupełniana jest najpierw ich wspólna część nazwy, a po kolejnym wciśnięciu przycisku `TAB` wyświetlana jest lista pakietów o danym początku nazwy. Mechanizm ten jest bardzo powszechny w ROSie (działa dla wielu poleceń nie tylko `roscd`), dlatego warto z niego korzystać - zwiększa to szybkość pracy, zwalnia z obowiązku pamiętania dokładnych nazw wszystkich używanych pakietów oraz zmniejsza ilość literówek.

## Pakiety ROS

Oprogramowanie w systemie ROS podzielone jest na pakiety. Pakiet może zawierać kilka powiązanych ze sobą programów, np. pakiet do obsługi kamer może zawierać program do komunikacji z kamerami USB, drugi do komunikacji z kamerami podłączanymi przez Ethernet, trzeci do kalibracji kamer.

W ROSie od wersji *Groovy* wykorzystywany jest system budowania pakietów `catkin`. Nazwa ta będzie pojawiać się często podczas wykorzystywania ROSa. Na laboratoriach nie będą omawiane starsze wersje pakietów ani sposoby migracji do nowej wersji.

Pakiet typu `catkin` musi spełniać kilka warunków:

* pakiet musi zawierać zgodny z systemem `catkin` opis w postaci pliku `package.xml`,
* pakiet musi zawierać plik `CMakeLists.txt`, opisujący sposób budowania pakiety z wykorzystaniem narzędzia `cmake`, oczywiście w zakresie zgodnym z systemem `catkin`,
* w każdym folderze może być tylko jeden pakiet (nie mogą występować pakiety zagnieżdżone jak również kilka pakietów nie może współdzielić tego samego folderu).

Najprostszy możliwy pakiet może wyglądać następująco:

```
my_package/
  CMakeLists.txt
  package.xml
```

Zalecanym sposobem pracy z pakietami `catkin` jest zapisywanie ich w katalogu przestrzeni roboczej stworzonej za pomocą `catkin_init_workspace`. Prosty katalog roboczy może wyglądać następująco:

```
workspace_folder/        -- WORKSPACE
  src/                   -- SOURCE SPACE
    CMakeLists.txt       -- 'Toplevel' CMake file, provided by catkin
    package_1/
      CMakeLists.txt     -- CMakeLists.txt file for package_1
      package.xml        -- Package manifest for package_1
    ...
    package_n/
      CMakeLists.txt     -- CMakeLists.txt file for package_n
      package.xml        -- Package manifest for package_n
```

Warto zasygnalizować, że pakiety mogą wymagać do uruchomienia lub kompilacji innych pakietów `catkin`. Niespełnienie zależności będzie skutkowało wyświetleniem odpowiednich komunikatów. Polecenie `catkin_make` umożliwia budowanie programów z poziomu wiersza poleceń.

## Węzły ROS (ang. *node*)

W pakiecie ROSa może znajdować się kilka programów. Nazywane są one węzłami. W języku angielskim słowo *node* oznacza węzeł lub wierzchołek sieci, co dobrze oddaje naturę współpracy tych programów wewnątrz ROSa - każdy z  nich stanowi węzeł powiązany kilkoma nitkami z innymi węzłami sieci. 

Zapoznajmy się więc z podstawowymi elementami tej sieci. Są to:

* **węzły** – pliki wykonywalne wykorzystujące ROSa do komunikacji z innymi węzłami,
* **wiadomości** (messages) – struktury danych wykorzystywane podczas wysyłania/subskrybowania informacji,
* **tematy** (topics) – kanały komunikacji. Każdy węzeł może wysyłać wiadomości na wybrany kanał komunikacyjny lub pobierać je z niego,
* **master** – proces główny, pozwalający węzłom na wzajemną komunikację poprzez zarządzanie nazwami,
* **rosout** – wyjście ROSa, odpowiednik wykorzystywanego w C++ `std::cout` oraz `std::cerr`,

Węzeł jest niczym więcej niż plikiem wykonywalnym (programem) znajdującym się w pakiecie ROSa. Węzły ROSa, oprócz kanałów komunikacyjnych, mogą również udostępniać lub wykorzystywać serwisy, czyli komunikację synchroniczną (blokującą).

Biblioteki klienckie ROSa umożliwiają wzajemna komunikacje pomiędzy węzłami napisanymi w różnych językach programowania. Dostępne są:

* `roscpp` – biblioteka w języku C++,
* `rospy` – biblioteka w języku Python (nieużywana w trakcie laboratorium).

## Rdzeń ROSa (`roscore`)

Rdzeń ROSa (`roscore`) – `master` + `rosout` + (niewykorzystywany w czasie laboratorium) serwer parametrów.


Rdzeń ROSa jest pierwszą rzeczą, jaką należy uruchomić przy pracy z ROSem. Służy do tego polecenie:

```
roscore
```

Po jego uruchomieniu w konsoli zostanie wyświetlony tekst zbliżony do poniższego:

![`roscore`](figures/roscore.png)

Po uruchomieniu `roscore`'a możemy przyjrzeć się prostym narzędziom do analizy stanu systemu ROS.

> **Uwaga**: programy uruchomione w terminalu zatrzymuje się skrótem klawiszowym `Ctrl+C`.

## Inspekcja uruchomionych węzłów `rosnode`

Aby zobaczyć listę uruchomionych węzłów należy w terminalu wpisać polecenie:

```
rosnode list
```

W tym wypadku wyjście będzie wyglądało następująco:

![rosnode list](figures/rosnode_list_2.png)

Widać, że uruchomiony jest tylko jeden węzeł `rosout`.  Aby uzyskać więcej informacji o uruchomionym procesie, należy wykorzystać polecenie `rosnode info`. Na przykład:

```
rosnode info /rosout
```

Wyświetli ono podstawowe informacje, które mogą być przydatne podczas debugowania, takie jak subskrybowane oraz publikowane wiadomości. Przykładowe wyjście wygląda następująco:

![`rosnode info`](figures/rosnode_info_2.png)

## Uruchamianie węzłów ROSa (`rosrun`)

Do uruchamiania kolejnych węzłów służy polecenie `rosrun`. Pozwala ono na uruchomienie węzła znajdującego się w danym pakiecie bez dokładnej wiedzy o jej lokalizacji. Ma ono następującą składnię:

```
rosrun [nazwa_pakietu] [nazwa_węzła]
```

Przy wpisywaniu obu nazw można skorzystać z auto-uzupełniania (klawisz `TAB`). Przykładowo uruchomimy teraz ćwiczeniowy robotyczny symulator żółwia, `TurtleSim`. Wpisujemy więc polecenie:

```
rosrun turtlesim turtlesim_node
```

Wyświetli się wtedy okno symulatora:

![TurtleSim](figures/turtlesim.png)

Jeśli teraz sprawdzimy listę node'ów poleceniem:

rosnode list

To zobaczymy na liście uruchomiony program:

```
/rosout
/turtlesim
```

## Inspekcja kanałów wiadomości

Ang. *topic* to kanał komunikacji nieblokującej typu nadawca odbiorca, po którym komunikują się ze sobą węzły. Aby zrozumieć ich działanie uruchomimy drugi program, sterujący żółwiem widocznym w naszym symulatorze. W tym celu wpisujemy komendę:

```
rosrun turtlesim turtle_teleop_key
```

Teraz możemy sterować żółwiem za pomocą strzałek na klawiaturze (jeśli żółw się nie rusza, to należy się upewnić, że okno terminalu z programem `turtle_teleop_key` jest zaznaczone).

![TurtleSim teleop](figures/turtlesim_teleop_2.png)

## Wizualizacja komunikacji (`rqt_graph`)

Widzimy więc, że komunikacja pomiędzy programami działa. Teraz przyjrzymy się dokładniej jej mechanizmom.

Węzły `turtlesim_node` oraz `turtle_teleop_key` komunikują się ze sobą poprzez *topic* ROSa. Node `turtle_teleop_key` publikuje komendy sterowania wydawane przez użytkownika, podczas gdy `turtlesim_node` subskrybuje je. Aby wygodnie przyjrzeć się sieci połączeń wykorzystamy narzędzie `rqt_graph`. Wizualizuje ono aktualną sieć połączeń pomiędzy węzłami. Uruchamiamy je poleceniem:

```
rqt_graph
```

Zobaczymy następujący widok:

![`rqt_graph`](figures/rqt_graph.png)

Widzimy wyraźnie, że komunikacja odbywa się poprzez kanał `/turtle1/cmd_vel`. Węzeł `/teleop_turtle` publikuje wiadomości, a węzeł `/turtlesim` je odbiera, co pokazuje strzałka.

## Inspekcja komunikacji w konsoli (`rostopic`)

Narzędzie `rostopic` ma kilka wariantów użycia. Zacznijmy od najprostszego, czyli od odsłuchania wiadomości publikowanych na danym kanale. W tym celu w terminalu wpisujemy polecenie:

```
rostopic echo /turtle1/cmd_vel
```

Po wpisaniu tego polecenia najprawdopodobniej nie wyświetla się żadne informacje, ponieważ nie sterujemy obecnie naszym żółwiem i sterowania nie są publikowane. Wystarczy jednak poruszyć żółwiem, aby wyświetlone zostały informacje zbliżone do poniższych:

```
linear: 
  x: 2.0
  y: 0.0
  z: 0.0
angular: 
  x: 0.0
  y: 0.0
  z: 0.0
---
linear: 
  x: 2.0
  y: 0.0
  z: 0.0
angular: 
  x: 0.0
  y: 0.0
  z: 0.0
---
```

Spójrzmy teraz ponownie na widok połączeń w rqt_graph. Po wciśnięciu przycisku odświeżenia będzie on wyglądał następująco:

![`rqt_graph`](figures/rqt_graph_2.png)

Widać, że proces `rostopic echo`, zaznaczony tutaj na czerwono, również zasubskrybował kanał `/turtle1/cmd_vel`.

Narzędzie `rostopic` może służyć również do wyświetlenia listy wszystkich kanałów aktywnych w systemie. Aby ja wyświetlić wpisujemy polecenie:

```
rostopic list
```

Aby komunikacja między węzłami mogła dojść do skutku węzły publikujący i subskrybujący muszą wykorzystywać ten sam typ wiadomości. Oznacza to, że typ kanału komunikacyjnego definiowany jest przez typ wiadomości na nim publikowanych. Może on być sprawdzony za pomocą polecenie `rostopic type` (lub bardziej dokładnego `rostopic info`). W naszym przykładzie możemy wpisać:

```
rostopic type /turtle1/cmd_vel
```

W efekcie otrzymamy typ wiadomości:

```
geometry_msgs/Twist
```

Jeszcze nie znamy tego typu wiadomości, więc chcielibyśmy poznać nie tylko jego nazwę, ale i strukturę, W tym celu wpisujemy:

```
rosmsg show geometry_msgs/Twist
```

I otrzymujemy strukturę wiadomości:

```
geometry_msgs/Vector3 linear
  float64 x
  float64 y
  float64 z
geometry_msgs/Vector3 angular
  float64 x
  float64 y
  float64 z
```

Jak widać typowa komenda prędkości w ROSie składa się z prędkości liniowej i kątowej, z których każda wyrażona jest poprzez jej składowe 3D. Oczywiście większość robotów mobilnych wykorzystuje tylko niektóre z nich. W trakcie tego laboratorium będą to składowa `x` prędkości liniowej oraz składowa `z` prędkości kątowej. Z powyższą wiedzą możemy przejść do napisania korzystających z ROSa programów w języku C++.
