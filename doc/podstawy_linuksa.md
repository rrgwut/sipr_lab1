## Podstawy Linuksa

ROS jest najczęściej instalowany na systemie operacyjnym Ubuntu (Linux). Praca z ROS wymaga opanowania podstaw obsługi konsoli tekstowej (terminala).

W ramach ćwiczeń będziemy korzystali z terminala `yakuake`, który uruchamia się automatycznie po starcie systemu, pozostaje jednak schowany. Jego rozwinięcie (lub ponowne schowanie) następuje po naciśnięciu klawisza **F12**.

![Terminal yakuake](figures/yakuake.png)

`yakuake` wyświetlany jest na górze ekranu. Umożliwia obsługę wielu kart (pasek na dole) oraz podziały okna w pionie (`Ctrl+Shift+(`) oraz poziomie (`Ctrl+Shift+)`). Przykładowy podział przedstawia poniższy zrzut ekranu:

![Terminal yakuake - podział ekranu](figures/yakuake_podzial.png)

Do poruszania się w strukturze plików wykorzystywane są następujące komendy:

| Polecenie | Opis |
| --------- | ---- |
| `ls` | Wyświetla listę plików i katalogów w obecnym folderze |
| `ls -a` | Wyświetla listę wszystkich (w tym ukrytych) plików i katalogów w obecnym folderze |
| `mkdir nowy_katalog` | Tworzy katalog o nazwie `nowy_katalog` w katalogu bieżącym |
| `cd` | Change directory – zmiana katalogu bieżącego. Przy braku argumentów oznacza powrót do katalogu domowego użytkownika. |
| `cd ~` | Przejście do katalogu domowego użytkownika | 
| `cd ./Dokumenty` | Przejście do katalogu `Dokumenty` (o ile taki katalog znajduje się w katalogu bieżącym |
| `cd ..` | Przejście do katalogu nadrzędnego 
| `pwd` | Print working directory – wypisanie pełnej ścieżki bieżącego katalogu roboczego. |

W terminalu można również uruchamiać programy posiadające interfejs graficzny. W ten sposób uruchamiana będzie również większość narzędzi wykorzystywanych przy pracy z robotami.
