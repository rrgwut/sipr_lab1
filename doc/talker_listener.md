# Tworzenie własnego pakietu w C++

W tej części instrukcji jak stworzyć własny pakiet ROSa z dwoma węzłami napisanymi w języku C++ umożliwiającymi publikowanie i odbieranie prostej wiadomości na utworzonym przez nas kanale wiadomości.

## Utworzenie pakietu (`catkin_create_pkg`)

Kody źródłowe pakietów umieszczamy w podkatalogu `src` naszej przestrzeni roboczej `sipr_ws`. Wchodzimy do katalogu poleceniami:

```
roscd
```

```
cd ../src
```

Do tworzenia pakietów ROSa służy polecenie `catkin_create_pkg` o składni:

```
catkin_create_pkg <nazwa_pakietu> <pakiet_wymagany_1> <pakiet_wymagany_2> ...
```

w którym najpierw podajemy nazwę nowego pakietu, a następnie (opcjonalnie) listę pakietów wymaganych przez nasz pakiet.

W ramach ćwiczenia przygotujemy pakiet `sipr_lab1_talker_listener` zależny od pakietów `roscpp` (API ROSa dla języka C++) i `std_msgs` (definicje standardowych wiadomości):

```
catkin_create_pkg sipr_lab1_talker_listener roscpp std_msgs
```

Za pomocą polecenia `tree` możemy zobaczyć co zawiera nowy pakiet:

```
tree sipr_lab1_talker_listener
```

![tree `sipr_lab1_talker_listener`](figures/catkin_create_pkg_tree.png)

Nowo stworzony pakiet budujemy poleceniem **`catkin_make` z poziomu katalogu `sipr_ws`** (nie `sipr_ws/src` ani `sipr_ws/src/sipr_lab1_talker_listener`):

```
cd ~/sipr_ws
```

```
catkin_make
```

## Praca z kodem źródłowym w Visual Studio Code

Do tworzenia kodu źródłowego potrzebne jest środowisko programistyczne. Tutaj będziemy korzystać z Visual Studio Code, które jest zainstalowane w systemie razem z wtyczką do obsługi ROSa.

Uruchomienie VS Code odbywa się z terminala poleceniem `code`, po którym podajemy nazwę katalogu naszej przestrzeni roboczej.

Wchodzimy do katalogu domowego użytkownika `sipr`:

```
cd
```

i uruchamiamy VS Code:

```
code sipr_ws
```

W drzewie plików możemy odnaleźć katalog pakietu `sipr_lab1_talker_listener`. 

![VS Code `sipr_ws`](figures/vscode_1.png)

Przestrzeń roboczą można zbudować za pomocą skrótu klawiszowego `Ctrl+Shift+B` po czym z listy wybieramy opcję `catkin_make: build`.

![VS Code build](figures/vscode_build.gif)

W podkatalogu `src` naszego pakietu, czyli w `~/sipr_ws/src/sipr_lab1_talker_listener/src`, tworzymy dwa pliki `talker.cpp` i `listener.cpp`.

![VS Code nowe pliki](figures/vscode_nowe_pliki.gif)

## Implementacja węzłów `talker` i `listener`

## Implementacja węzła `talker`

W pliku `talker.cpp` należy umieścić poniższy kod:

```c++
#include "ros/ros.h"
#include "std_msgs/String.h"

#include <sstream>

int main(int argc, char **argv)
{
  ros::init(argc, argv, "talker");
  ros::NodeHandle n;
  ros::Publisher chatter_pub = n.advertise<std_msgs::String>("chatter", 1000);
  ros::Rate loop_rate(10);
  int count = 0;
  while (ros::ok())
  {
    std_msgs::String msg;

    std::stringstream ss;
    ss << "hello world " << count;
    msg.data = ss.str();
    ROS_INFO("%s", msg.data.c_str());

    chatter_pub.publish(msg);

    ros::spinOnce();

    loop_rate.sleep();
    ++count;
  }
  return 0;
}
```

Przejdźmy do wyjaśnienia kolejnych fragmentów tego kodu.

```c++
#include "ros/ros.h"
```

Jest to dyrektywa dołączająca podstawowy plik nagłówkowy ROSa, zawierający większość funkcji ogólnego przeznaczenia.

```c++
#include "std_msgs/String.h"
```

Dołącza plik nagłówkowy wiadomości ROSowej typu `String`, znajdującej się w paczce wiadomości standardowych `std_msgs`.

```c++
  ros::init(argc, argv, "talker");
```

W tej linii inicjalizujemy komunikację naszego węzła z ROSem. Nadajemy mu przy tym nazwę `talker`, która musi być unikalna w całym systemie robota.

```c++
  ros::NodeHandle n;
```

Tworzy uchwyt do węzła ROSa (obiekt udostępniający metody do zarządzania węzłem). Za jego pomocą przypisujemy obiekty subskrybujące i publikujące do kanałów komunikacyjnych.

```c++
  ros::Publisher chatter_pub = n.advertise<std_msgs::String>("chatter", 1000);
```

W tej linii informujemy ROSa, że chcemy publikować wiadomości typu `<std_msgs::String>` na kanale `/chatter`. Dzięki temu wszystkie węzły nasłuchujące kanału `/chatter` będą nasłuchiwać również informacji publikowanych przez nasz węzeł. 

Drugi argument to rozmiar kolejki wiadomości. W tym wypadku mówi on, że jeśli będziemy publikować wiadomości zbyt szybko, to w buforze zostanie zgromadzonych do `1000` wiadomości, nim zaczną być one pomijane – warto więc pamiętać o ustawieniu odpowiedniego rozmiaru bufora.

Funkcja `advertise` zwraca obiekt typu `ros::Publisher`, który ma dwa zadania:

* umożliwia publikowanie wiadomości poprzez funkcję `publish`,
* automatycznie informuje o końcu nadawania po wykonaniu jego destruktora.

```c++
  ros::Rate loop_rate(10);
```

Obiekt typu `ros::Rate` pozwala na określenie częstotliwości, z jaką wykonywać będzie się pętla główna naszego programu. Mierzy on czas jaki upłynął od zakończenia poprzedniego wywołania funkcji `Rate::sleep`, a następnie zatrzymuje wykonywanie programu na obliczony na tej podstawie czas. W tym wypadku chcemy, aby program działał z częstotliwością 10 Hz.

```c++
  while (ros::ok())
  {
```

Domyślnie `roscpp` umożliwia przerywanie działania programu poprzez kombinacje klawiszy `Ctrl+C`. Skutkuje to właśnie zwróceniem wartości `false` przez funkcję `ros::ok()`, co przerywa wykonywanie pętli głównej.

```c++
    std_msgs::String msg;

    std::stringstream ss;
    ss << "hello world " << count;
    msg.data = ss.str();
```

W tym fragmencie kodu deklarujemy zmienną typu `std_msgs::String`, a następnie wypełniamy jej pola danymi. W tym wypadku wiadomość ma tylko jedno pole, mianowicie `data` typu `string`. W dalszej części laboratorium będziemy wykorzystywać bardziej złożone wiadomości.

```c++
    ROS_INFO("%s", msg.data.c_str());
```

`ROS_INFO` i inne funkcje z tej dziedziny (`ROS_WARN`, `ROS_ERROR`) są odpowiednikami `std::cout`. W węzłach można wykorzystywać standardowe wyjście `std::cout`, ale jeśli interesuje nas np. dokładny czas wyświetlenia komunikatu czy też wypisywanie błędów kolorem czerwonym, a ostrzeżeń żółtym, to wygodniej jest użyć wersji ROSowej.

```c++
    chatter_pub.publish(msg);
```

W tej linii wykonuje się wysłanie wiadomości do wszystkich węzłów połączonych z kanałem na którym publikujemy.

```c++
    ros::spinOnce();
```

Wywołanie tej funkcji nie jest w wypadku tak prostego programu konieczne, gdyż odpowiada ona za wywołanie funkcji obsługujących wiadomości przychodzące, a powyższy program żadnych wiadomości przychodzących nie subskrybuje. Można jednak dodać ja do kodu na wypadek, gdybyśmy chcieli w przyszłości rozwijać nasz program oraz dla zachowania dobrych praktyk programowania w ROSie. Jeśli funkcja ta nic nie robi, to nie stanowi mierzalnego narzutu obliczeniowego.

```c++
    loop_rate.sleep();
```

Funkcję tą wywołujemy, aby wstrzymać proces na czas potrzebny do uzyskania zadanej przez nas częstości działania, czyli 10 Hz.

A więc w skrócie działanie programu składa się z następujących etapów:

* inicjalizacja ROSa,
* poinformowanie mastera, że zamierzamy publikować wiadomości typu `std_msgs::String` na kanale `/chatter`,
* publikowanie wiadomości na kanale `/chatter` z częstotliwością 10 razy na sekundę.

## Implementacja węzła `listener`

W pliku `listener.cpp` należy umieścić poniższy kod:

```c++
#include "ros/ros.h"
#include "std_msgs/String.h"

void chatterCallback(const std_msgs::String::ConstPtr& msg)
{
  ROS_INFO("I heard: [%s]", msg->data.c_str());
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "listener");

  ros::NodeHandle n;

  ros::Subscriber sub = n.subscribe("chatter", 1000, chatterCallback);

  ros::spin();

  return 0;
}
```

Omówmy teraz kolejne fragmenty tego kodu, omijając te wyjaśnione wcześniej.

```c++
void chatterCallback(const std_msgs::String::ConstPtr& msg)
{
  ROS_INFO("I heard: [%s]", msg->data.c_str());
}
```

Jest to funkcja, która będzie wywołana gdy nowa wiadomość zostanie opublikowana na kanale `/chatter`.

```c++
  ros::Subscriber sub = n.subscribe("chatter", 1000, chatterCallback);
```

W tej linii informujemy ROSa, że chcemy subskrybować wiadomości pojawiające się na kanale `/chatter`. Drugi argument to rozmiar bufora wiadomości – w tym wypadku, jeśli będziemy przetwarzać wiadomości zbyt wolno, to w buforze zostanie zakolejkowanych do `1000` wiadomości nim zostaną one stracone. Podany tutaj rozmiar bufora nie musi być identyczny jak ten, który podaliśmy pisząc node publikujący. Trzeci argument to nazwa funkcji, która ma być wywoływana po otrzymaniu nowej wiadomości.

Funkcja `subscribe` zwraca obiekt typu `ros::Subscriber`, który wprawdzie nie jest jawnie wykorzystywany w żadnym innym miejscu kodu, ale nie może zostać zniszczony – wywołanie jego destruktora spowoduje zakończenie subskrybowania danych.

```c++
  ros::spin();
```

Funkcja `ros::spin()` wchodzi w pętlę wywołującą funkcje obsługi przychodzących wiadomości tak szybko jak to możliwe. Nie należy jednak obawiać się jej stosowania, ponieważ nie obciąży ona CPU znacząco, jeśli ilość przychodzących wiadomości jest mała. Funkcja zakończy działanie, gdy `ros::ok()` zwróci wartość `false` (czyli np. po wciśnięciu kombinacji `Ctrl+C` w terminalu).

Podsumujmy więc kolejne kroki działania programu:

* inicjalizacja ROSa,
* zasubskrybowanie kanału `/chatter`,
* oczekiwanie na nadejście wiadomości, 
* gdy wiadomość przyjdzie wywoływana jest funkcja `chatterCallback()`.

## Konfiguracja procesu kompilacji (`CMakeLists.txt`)

Samo stworzenie plików `talker.cpp` i `listener.cpp` nie powoduje powstania plików węzłów ROSa w procesie kompilacji. Aby tak się stało należy dodać odpowiednie polecenia w pliku `sipr_lab1_talker_listener/CMakeLists.txt`, które poinformują `catkin_make` co zbudować.

Plik `sipr_lab1_talker_listener/CMakeLists.txt` utworzony przez polecenie `catkin_create_pkg` jest obszerny i zawiera wiele przykładów z komentarzami. Warto się z nimi zapoznać, jednak na potrzeby ćwiczenia całą zawartość pliku `sipr_lab1_talker_listener/CMakeLists.txt` zastąpimy wersją minimalną. 

```cmake
cmake_minimum_required(VERSION 2.8.3)
project(sipr_lab1_talker_listener)

# Find catkin and any catkin packages
find_package(catkin REQUIRED COMPONENTS roscpp std_msgs)

# Declare a catkin package
catkin_package()

# Build talker and listener
include_directories(include ${catkin_INCLUDE_DIRS})

add_executable(talker src/talker.cpp)
target_link_libraries(talker ${catkin_LIBRARIES})

add_executable(listener src/listener.cpp)
target_link_libraries(listener ${catkin_LIBRARIES})
```

Teraz możemy uruchomić budowanie skrótem `Ctrl+Shift+B` w VS Code lub poleceniem `catkin_make` w terminalu w katalogu `~/sipr_ws`.

## Uruchomienie węzłów `talker` i `listener`

Podzielmy terminal `yakuake` na trzy części (okna). W kolejnych oknach uruchamiamy:

```
roscore
```

```
rosrun sipr_lab1_talker_listener talker
```

```
rosrun sipr_lab1_talker_listener listener
```

![`sipr_lab1_talker_listener` run](figures/talke_listener_run.gif)
