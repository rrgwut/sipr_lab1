# TF – układy współrzędnych w systemie ROS

## Wstęp

W robotyce układy współrzędnych i przejścia między nimi odgrywają znaczącą rolę. Wyznaczanie równań macierzy transformacji jest jednak zadaniem żmudnym oraz dającym wiele możliwości do popełnienia błędu. Dlatego też w systemie ROS do przejścia pomiędzy nimi wykorzystywany jest mechanizm drzewa układów współrzędnych TF.

Poniższa instrukcja jest przetłumaczona ze strony <http://wiki.ros.org/tf/Tutorials>.

## TurtleSim demo

Aby uruchomić demo wpisujemy komendę:

```
roslaunch turtle_tf turtle_tf_demo.launch
```

 Widzimy, że turtlesim uruchomił się z włączonymi dwoma żółwiami:

 ![turtle_tf_start](figures/turtle_tf_start.png)

 Za pomocą strzałek na klawiaturze (w aktywnym oknie terminala, z którego uruchomiony był `roslaunch`) możemy sterować jednym z żółwi, natomiast drugi z nich będzie za nim podążał:

 ![turtle_tf_drive](figures/turtle_tf_drive.png)

Powyższe demo wykorzystuje bibliotekę TF do utworzenia trzech układów współrzędnych:

* world
* turtle1
* turtle2

Demo wykorzystuje *tf broadcaster*, aby publikować wzajemne położenie układów oraz *tf listener*, aby obliczać pozycję jednego żółwia w układzie drugiego i w ten sposób go śledzić.

Aby zobaczyć, co dzieję się w ROSie od strony układów współrzędnych, wykorzystamy zestaw dedykowanych narzędzi.

Narzędzie `view_frames` tworzy diagram układów publikowanych na kanale `/tf` w ROSie. Uruchamiamy je komendą:

```
rosrun tf view_frames
```

I uzyskujemy następujące wyjście:

```
Transform Listener initing
Listening to /tf for 5.000000 seconds
Done Listening
dot - Graphviz version 2.16 (Fri Feb  8 12:52:03 UTC 2008)

Detected dot version 2.16
frames.pdf generated
```

Program ten wykorzystuje *tf listener*, aby utworzyć schemat wszystkich układów. W naszym wypadku plik PDF będzie wyglądał tak:

 ![view_frames](figures/view_frames_2.png)

Możemy tutaj zobaczyć trzy układy publikowane przez TF: `world`, `turtle1` oraz `turtle2`. Możemy również zobaczyć, że w powyższej hierarchii układ `world` jest rodzicem układów `turtle1` i `turtle2`.W celu ułatwienia debugowania `view_frames` wypisuje również informacje o częstości publikowania pozycji układów, długości bufora oraz czasie otrzymania ostatniego układu.

Zapisanie wyniku do pliku PDF nie zawsze jest wygodne. Zamiast tego możemy wykorzystać narzędzie `rqt_tf_tree`, pozwalające na podgląd naszego drzewa układów współrzędnych w czasie rzeczywistym. Uruchamiamy je poleceniem:

```
rosrun rqt_tf_tree rqt_tf_tree
```

Uzyskamy następujący widok:

![rqt_tf_tree](figures/snapshot_rqt_tree_turtle_tf.png)

Powyższe diagramy informują nas o nazwach istniejących układów współrzędnych oraz o relacjach rodzic-dziecko w drzewie układów. Aby uzyskać informacje o transformacji pomiędzy dwoma wybranymi układami współrzędnych, wykorzystujemy narzędzie `tf_echo`:

```
rosrun tf tf_echo [reference_frame] [target_frame]
```

Przyjrzyjmy się transformacji z układu turtle1 do układu turtle2. Jest ona obliczana jako:

![Transformacje](figures/tf.png)

Polecenie:

```
rosrun tf tf_echo turtle1 turtle2
```

Daje informacje o wzajemnym położeniu układów w kolejnych chwilach czasu:

```
At time 1416409795.450
- Translation: [0.000, 0.000, 0.000]
- Rotation: in Quaternion [0.000, 0.000, 0.914, 0.405]
            in RPY [0.000, -0.000, 2.308]
At time 1416409796.441
- Translation: [0.000, 0.000, 0.000]
- Rotation: in Quaternion [0.000, 0.000, 0.914, 0.405]
            in RPY [0.000, -0.000, 2.308]
At time 1416409797.450
- Translation: [0.000, 0.000, 0.000]
- Rotation: in Quaternion [0.000, 0.000, 0.914, 0.405]
            in RPY [0.000, -0.000, 2.308]
```

Sterując jednym z żółwi można zobaczyć, że wartość transformacji ulega zmianie – co jest oczywiste, ponieważ żółwie poruszają się względem siebie.

Układy TF można jak wiele innych rzeczy oglądać w programie `rviz`:

```
rosrun rviz rviz -d `rospack find turtle_tf`/rviz/turtle_rviz.rviz
```

> Fragment polecenia ``` `rospack find turtle_tf`/rviz/turtle_rviz.rviz ``` używa polecania `rospack find` pozwalającego odnaleźć katalog pakietu o danej nazwie, tutaj `turtle_tf`.

![tf rviz](figures/turtle_tf_rviz.png)

## Własny węzeł `tf_broadcaster`

Własne węzły `tf_broadcaster` i `tf_listener` (o nim dalej w instrukcji) umieścimy w nowym pakiecie `sipr_lab1_tf`, który należy teraz utworzyć w przestrzeni roboczej `sipr_ws`.

```
cd ~/sipr_ws/src
```

```
catkin_create_pkg sipr_lab1_tf roscpp tf turtlesim
```

Kompilujemy przestrzeń roboczą:

```
cd ..
```

```
catkin_make
```

### Implementacja węzła `tf_broadcaster`

Na początek włączamy środowisko VS Code:

```
cd ~
```

```
code sipr_ws
```

W tym ćwiczeniu pokazane będzie, jak dodać nowy układ współrzędnych do struktury drzewa TF. Wykorzystamy w tym celu pozycje robotów publikowane przez symulator `turtlesim` (dla rzeczywistego robota te pozycje należałoby wyliczyć np. z odometrii). Kod węzła implementujemy w pliku `sipr_lab1_tf/src/tf_broadcaster.cpp` (należy taki plik stworzyć), który będzie wyglądał następująco:

```c++
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <turtlesim/Pose.h>

std::string turtle_name;

void poseCallback(const turtlesim::PoseConstPtr& msg){
  static tf::TransformBroadcaster br;
  tf::Transform transform;
  transform.setOrigin( tf::Vector3(msg->x, msg->y, 0.0) );
  tf::Quaternion q;
  q.setRPY(0, 0, msg->theta);
  transform.setRotation(q);
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world", turtle_name));
}

int main(int argc, char** argv){
  ros::init(argc, argv, "my_tf_broadcaster");
  if (argc != 2){ROS_ERROR("need turtle name as argument"); return -1;};
  turtle_name = argv[1];

  ros::NodeHandle node;
  ros::Subscriber sub = node.subscribe(turtle_name+"/pose", 10, &poseCallback);

  ros::spin();
  return 0;
};
```

Przeanalizujemy teraz kolejno kod odpowiedzialny za publikowanie pozycji robota w drzewie TF.

```c++
#include <tf/transform_broadcaster.h>
```

Pakiet TF dostarcza gotową klasę `TransformBroadcaster`, umożliwiającą łatwe publikowanie transformacji. Aby z niej korzystać, musimy dołączyć plik nagłówkowy `tf/transform_broadcaster.h`.

```c++
  static tf::TransformBroadcaster br;
```

W tym miejscu tworzymy obiekt klasy `TransformBroadcaster`, który będziemy wykorzystywali później do publikowania transformacji w systemie ROS.

```c++
  tf::Transform transform;
  transform.setOrigin( tf::Vector3(msg->x, msg->y, 0.0) );
  tf::Quaternion q;
  q.setRPY(0, 0, msg->theta);
```

W tym miejscu tworzymy obiekt reprezentujący transformację oraz przenosimy informację o pozycji 2D robota do struktury pełnej transformacji 3D.

```c++
  transform.setRotation(q);
```

W powyższej linii ustawiamy rotację transformacji.

```c++
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world", turtle_name));
```

W tej linii wykonywana jest zasadnicza część zadania. Opublikowanie transformacji za pomocą tf `TransformBroadcaster`'a wymaga podania czterech argumentów:

1. wartości liczbowych transformacji pomiędzy układami,
2. czasu do którego odnosi się transformacja – w naszym wypadku będzie to "teraz",
3. nazwy układu współrzędnego będącego w tej transformacji rodzicem – czyli tutaj `world`,
4. nazwy układu dziecka, w tym wypadku jest to nazwa żółwia.

### Kompilacja `tf_broadcaster`

W celu zbudowania węzła `tf_broadcaster` musimy zmodyfikować plik `sipr_lab1_tf/CMakeLists.txt` utworzony przez polecenie `catkin_create_pkg` wpisując treść:

```cmake
cmake_minimum_required(VERSION 2.8.3)
project(sipr_lab1_tf)

# Find catkin and any catkin packages
find_package(catkin REQUIRED COMPONENTS roscpp tf turtlesim)

# Declare a catkin package
catkin_package()

# Build talker and listener
include_directories(include ${catkin_INCLUDE_DIRS})

add_executable(tf_broadcaster src/tf_broadcaster.cpp)
target_link_libraries(tf_broadcaster ${catkin_LIBRARIES})
```

Teraz możemy uruchomić budowanie skrótem `Ctrl+Shift+B` w VS Code lub poleceniem `catkin_make` w terminalu w katalogu `~/sipr_ws`.

### Uruchomienie `tf_broadcaster`

W celu przetestowania węzła `tf_broadcaster` musimy uruchomić kilka programów:

1. `roscore`
2. `rosrun turtlesim turtlesim_node`
3. `rosrun turtlesim turtle_teleop_key`
4. `rosrun sipr_lab1_tf tf_broadcaster`

Uruchamianie i obsługa wielu konsoli naraz jest uciążliwa. Dlatego skorzystamy tutaj po raz pierwszy z programu `roslaunch` uruchamiającego skrypty ROS, czyli pliki o rozszerzeniu `*.launch`.

Program `roslaunch` powoduje automatyczne uruchomienie `roscore`, a więc w skrypcie umieszczamy tylko instrukcje uruchamiające pozostałe węzły.

W tym celu w katalogu pakietu `sipr_lab1_tf` tworzymy podkatalog `launch`, w którym umieszczamy plik `turtle_tf_broadcaster.launch` o następującej treści:

```xml
<launch>
    <!-- Uruchomienie symulatora Turtlesim-->
    <node pkg="turtlesim" type="turtlesim_node" name="sim"/>

    <!-- Uruchomienie sterowania żółwiem z klawiatury -->
    <node pkg="turtlesim" type="turtle_teleop_key" name="teleop_key" output="screen"/>

    <!-- Ustawienia osi symulatora -->
    <param name="scale_linear" value="2" type="double"/>
    <param name="scale_angular" value="2" type="double"/>

    <!-- Uruchomienie węzła tf_broadcaster -->
    <node pkg="sipr_lab1_tf" type="tf_broadcaster"
            args="/turtle1" name="turtle1_tf_broadcaster" />
</launch>
```

![VS Code dodanie `launch`](figures/tf_launch.gif)

Skrypt uruchamiamy poleceniem:

```
roslaunch sipr_lab1_tf turtle_tf_broadcaster.launch
```

Efekt można sprawdzić uruchamiając w drugim oknie terminala polecenie:

```
rosrun tf tf_echo /world /turtle1
```

## Własny węzeł `tf_listener`

### Implementacja węzła `tf_listener`

Aby utworzyć program, który będzie sterował drugim żółwiem tak, aby podążał on za pierwszym, w pakiecie `sipr_lab1_tf` dodajemy plik `sipr_lab1_tf/src/tf_listener.cpp` z kodem:

```c++
#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/Twist.h>
#include <turtlesim/Spawn.h>

int main(int argc, char** argv){
  ros::init(argc, argv, "my_tf_listener");

  ros::NodeHandle node;

  ros::service::waitForService("spawn");
  ros::ServiceClient add_turtle =
    node.serviceClient<turtlesim::Spawn>("spawn");
  turtlesim::Spawn srv;
  add_turtle.call(srv);

  ros::Publisher turtle_vel =
    node.advertise<geometry_msgs::Twist>("turtle2/cmd_vel", 10);

  tf::TransformListener listener;

  ros::Rate rate(10.0);
  while (node.ok()){
    tf::StampedTransform transform;
    try{
      listener.lookupTransform("/turtle2", "/turtle1",
                               ros::Time(0), transform);
    }
    catch (tf::TransformException &ex) {
      ROS_ERROR("%s",ex.what());
      ros::Duration(1.0).sleep();
      continue;
    }

    geometry_msgs::Twist vel_msg;
    vel_msg.angular.z = 4.0 * atan2(transform.getOrigin().y(),
                                    transform.getOrigin().x());
    vel_msg.linear.x = 0.5 * sqrt(pow(transform.getOrigin().x(), 2) +
                                  pow(transform.getOrigin().y(), 2));
    turtle_vel.publish(vel_msg);

    rate.sleep();
  }
  return 0;
};
```

>**Uwaga!** W wypadku uzyskania po uruchomieniu błędu *"Lookup would require extrapolation into the past"* można skorzystać z alternatywnej wersji wywołania listener'a, który czeka przez 10 sekund na brakującą transformację:
>
>```c++
>try {
>    listener.waitForTransform(destination_frame, original_frame, >ros::Time(0), ros::Duration(10.0) );
>    listener.lookupTransform(destination_frame, original_frame, ?>ros::Time(0), transform);
>} catch (tf::TransformException ex) {
>    ROS_ERROR("%s",ex.what());
>}
>```

Wyjaśnienie istotnych fragmentów kodu:

```c++
#include <tf/transform_listener.h>
```

Pakiet TF dostarcza gotową klasę `TransformListener`, umożliwiającą łatwe subskrybowanie transformacji. Aby z niej korzystać, musimy dołączyć plik nagłówkowy `tf/transform_listener.h`.

```c++
  ros::service::waitForService("spawn");
  ros::ServiceClient add_turtle =
    node.serviceClient<turtlesim::Spawn>("spawn");
  turtlesim::Spawn srv;
  add_turtle.call(srv);
```

Powyższy kod pokazuje wykorzystanie komunikacji synchronicznej za pomocą serwisu o nazwie `/spawn`, który powoduje dodanie żółwia do symulatora `TurtleSim`.

```c++
  tf::TransformListener listener;
```

W tym miejscu tworzona jest instancja obiektu klasy `TransformListener`. W momencie utworzenia zaczyna od odbierać informacje o transformacjach publikowane na kanale `/tf`, i buforuje je do 10 sekund (jest to wartość domyślna, którą można zmienić podając czas buforowania jako parametr konstruktora). Obiekt ten, w związku z powyższym, nie może być deklarowany lokalnie, bo przy każdym wywołaniu funkcji bufor będzie zawsze tworzony od nowa, a więc będzie pusty, co spowoduje komunikaty o braku transformacji przy uruchomieniu. Najpopularniejszymi metodami na pokonanie tej trudności w programach mających więcej funkcji niz tylko `main()` jest zadeklarowanie obiektu jako składowa klasy lub jako zmienna globalna.

```c++
    try{
      listener.lookupTransform("/turtle2", "/turtle1",
                               ros::Time(0), transform);
    }
```

W tej linii wykonywana jest zasadnicza część zadania. Odpytujemy listener o transformacje pomiędzy dwoma interesującymi nas układami. Lista argumentów jest następująca:

1. Interesuje nas transformacja z układu `"/turtle2"`...
2. ... do układu `"/turtle1"`.
3. Jako trzeci argument podajemy czas, w którym chcemy znać transformację. Ponieważ interesuje nas ostatnie otrzymane przekształcenie, to podajemy wartość `ros::Time(0)`.
4. Ostatnim argumentem jest obiekt w którym chcemy przechowywać uzyskaną transformację.
Wszystko to znajduje się w bloku `try{...}catch` aby obsłużyć potencjalne wyjątki (a są one dosyć częste i bez tego powodowały by problemy z działaniem).

```c++
    geometry_msgs::Twist vel_msg;
    vel_msg.angular.z = 4.0 * atan2(transform.getOrigin().y(),
                                    transform.getOrigin().x());
    vel_msg.linear.x = 0.5 * sqrt(pow(transform.getOrigin().x(), 2) +
```

Powyżej uzyskana transformacja wykorzystywana jest do obliczenia nowych składowych liniowej i kątowej prędkości żółwia `turtle2`, podążającego za żółwiem `turtle1`. Prędkość ta publikowana jest na kanale `/turtle2/cmd_vel`, skąd czytana jest przez symulator w celu sterowania żółwiem.

### Kompilacja `tf_listener`

W celu zbudowania węzła `tf_listener` musimy dodać do pliku `sipr_lab1_tf/CMakeLists.txt` instrukcje:

```cmake
add_executable(tf_listener src/tf_listener.cpp)
target_link_libraries(tf_listener ${catkin_LIBRARIES})
```

Teraz możemy uruchomić budowanie skrótem `Ctrl+Shift+B` w VS Code lub poleceniem `catkin_make` w terminalu w katalogu `~/sipr_ws`.

### Uruchomienie węzła `tf_listener`

W katalogu `sipr_lab1_tf/launch` tworzymy plik `turtle_tf_demo.launch` o następującej treści:

```xml
<launch>
    <!-- Załączenie (uruchomienie) wcześniej napisanego skryptu turtle_tf_broadcaster.launch -->
    <include file="$(find sipr_lab1_tf)/launch/turtle_tf_broadcaster.launch"/>

    <!-- Uruchomienie węzła tf_listener sterującego drugim żółwiem -->
    <node pkg="sipr_lab1_tf" type="tf_listener"
            args="/turtle2" name="turtle2_tf_listener" />

    <!-- Uruchomienie węzła tf_broadcaster dla drugiego żółwia -->
    <node pkg="sipr_lab1_tf" type="tf_broadcaster"
            args="/turtle2" name="turtle2_tf_broadcaster" />
</launch>
```

Skrypt uruchamiamy poleceniem:

```
roslaunch sipr_lab1_tf turtle_tf_demo.launch
```

Po uruchomieniu napisanego programu zauważymy, że udało nam się odtworzyć funkcjonalność demo przedstawionego na początku instrukcji – drugi żółw podąża za sterowanym przez nas pierwszym.

> Zaraz po uruchomieniu może pojawić się pojedynczy błąd z `tf_listener`. Wynika to z faktu, że `tf_listener` został uruchomiony szybciej niż zaczął nadawać `tf_broadcaster` drugiego żółwia.
>
>![tf error](figures/tf_error.png)

## Dodawanie nowego układu współrzędnych

W poprzednich częściach odtwarzaliśmy funkcjonalność przedstawionego na początku demo. Teraz napiszemy program dodający dodatkowy układ współrzędnych. Jest to zadanie podobne do pisania poprzedniego `tf_broadcaster`'a, pokazuje jednak możliwości jakie niesie za sobą mechanizm TF.

Z wielu względów dobrze jest mieć w układzie robota liczne układy lokalne – np. łatwiej myśleć o odczytach skanera laserowego w układzie współrzędnych skanera. Dodawanie nowych układów pozwoli zdefiniować układ lokalny dla wszystkich sensorów, połączeń itp. Dzięki mechanizmowi TF nie skomplikuje to kodu naszego programu.

TF tworzy strukturę drzewa układów współrzędnych, nie pozwala na zamykanie pętli w strukturze układów. Oznacza to, ze każdy układ ma tylko jednego rodzica, może mieć natomiast wiele dzieci. Obecnie nasze drzewo TF zawiera trzy układy: `world`, `turtle1` oraz `turtle2`. Dwa ostatnie układy są dziećmi układu `world`.  Jeśli chcemy dodać nowy układ, to któryś z obecnych układów musi byc jego rodzicem, a nowy układ będzie dzieckiem.

Dodamy układ będący w stałej relacji względem `turtle1`, który będzie teraz pełnił rolę "marchewki" za którą będzie podążał drugi żółw (zamiast za pierwszym żółwiem).

![Drzewo TF z układem `carrot`](figures/carrot_tf_tree.png)

Aby wykonać nasze zadanie, do pakietu `sipr_lab1_tf` dodamy plik `tf_carrot_broadcaster.cpp` o następującej treści:

```c++
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>

int main(int argc, char** argv){
  ros::init(argc, argv, "carrot_tf_broadcaster");
  ros::NodeHandle node;

  tf::TransformBroadcaster br;
  tf::Transform transform;

  ros::Rate rate(10.0);
  while (node.ok()){
    transform.setOrigin( tf::Vector3(0.0, 2.0, 0.0) );
    transform.setRotation( tf::Quaternion(0, 0, 0, 1) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "turtle1", "carrot1"));
    rate.sleep();
  }
  return 0;
};
```

W powyższym kodzie kluczowy jest jeden fragment:

```c++
    transform.setOrigin( tf::Vector3(0.0, 2.0, 0.0) );
    transform.setRotation( tf::Quaternion(0, 0, 0, 1) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "turtle1", "carrot1"));
```

Tworzymy w nim nową transformację, z układu `turtle1` do układu `carrot1`. Układ `carrot1` jest przesunięty o dwa metry w lewo względem układu `turtle1`.

Pozostaje nam jeszcze tylko zmienić jedną linię w kodzie sterującym drugim żółwiem, w pliku `tf_listener.cpp`, z:

```c++
  listener.lookupTransform("/turtle2", "/turtle1", ros::Time(0), transform);
```

na:

```c++
  listener.lookupTransform("/turtle2", "/carrot1", ros::Time(0), transform);
```

tak, aby podążał on za nowym układem współrzędnych.

### Kompilacja `tf_carrot_broadcaster`

Do pliku `sipr_lab1_tf/CMakeLists.txt` należy dodać instrukcje:

```cmake
add_executable(tf_carrot_broadcaster src/tf_carrot_broadcaster.cpp)
target_link_libraries(tf_carrot_broadcaster ${catkin_LIBRARIES})
```

Następnie należy uruchomić budowanie skrótem `Ctrl+Shift+B` w VS Code lub poleceniem `catkin_make` w terminalu w katalogu `~/sipr_ws`.

### Uruchomienie węzła `tf_carrot_broadcaster`

W pliku `turtle_tf_demo.launch` dodajemy instrukcję uruchamiającą węzeł `tf_carrot_broadcaster`:

```xml
    <!-- Uruchomienie węzła tf_carrot_broadcaster  -->
    <node pkg="sipr_lab1_tf" type="tf_carrot_broadcaster"
            name="tf_carrot_broadcaster" />
```

Skrypt uruchamiamy poleceniem:

```
roslaunch sipr_lab1_tf turtle_tf_demo.launch
```

Po uruchomieniu naszych programów widzimy, że drugi żółw podąża teraz za punktem znajdującym się obok pierwszego. Jak widać wykorzystanie mechanizmu TF czyni takie zadania łatwymi.

## Trochę wiadomości o mechanizmie TF i czasie

Do tej pory uczyliśmy się tego jak TF przechowuje informacje o transformacjach pomiędzy układami współrzędnych. Drzewo transformacji zmienia się w czasie i dla każdej transformacji przechowywany jest stempel czasowy (o ile znajduje się ona jeszcze  buforze, o domyślnej długości 10 sekund). Do tej pory wykorzystywaliśmy funkcję `lookupTransform()`, aby uzyskać najnowszą dostępną transformację. Teraz nauczymy się jak uzyskać transformację w żądanej chwili czasu.

Przejdźmy więc do kodu sterującego drugim żółwiem (plik `tf_listener.cpp`). Najpierw dla uproszczenia zmieńmy ostatnio zmodyfikowana linię z powrotem na:

```c++
   try{
      listener.lookupTransform("/turtle2", "/turtle1", ros::Time(0), transform);
```

i sprawdźmy, czy wszystko działa:

* skompilujmy kod `Ctrl+Shift+B` w VS Code
* w terminalu włączmy skrypt

  ```
  roslaunch sipr_lab1_tf turtle_tf_demo.launch
  ```

Jak było powiedziane, wartość `ros::Time(0)` oznacza, że interesuje nas najnowsza dostępna transformacja. Spróbujmy zastąpić to wyrażenie poprzez czas jawnie oznaczający "teraz":

```c++
  try{
    listener.lookupTransform("/turtle2", "/turtle1",  
                             ros::Time::now(), transform);
```

Po skompilowaniu i ponownym urucomieniu programów naszym ocozm ukaże się taki widok:

```
[ERROR] [1287871653.885277559]: You requested a transform that is 0.018 miliseconds in the past, but the most recent transform in the tf buffer is 7.681 miliseconds old.                                                 
 When trying to transform between /turtle1 and /turtle2.  
....
```

Dzieje się tak dlatego, ze `tf_listener` posiada bufor, w którym zapamiętuje wszystkie informacje publikowane przez różne `tf_broadcaster`'y. Od chwili gdy `tf_broadcaster` wyśle transformacje potrzebny jest pewien czas by trafiła ona do bufora `tf_listener`'a (zwykle kilka milisekund). Ponadto transformacje wysyłane są tylko w określonych, dyskretnych chwilach czasowych. Jeśli więc chcemy uzyskać transformacje dla czasu `ros::Time::now()`, musimy zwykle poczekać te kilka milisekund. Na szczęście TF dostarcza wygodne narzędzie pozwalające nam odczekać, az transformacja będzie dostępna. Wystarczy dokonać następującej modyfikacji kodu:

```c++
  try{
    ros::Time now = ros::Time::now();
    listener.waitForTransform("/turtle2", "/turtle1",
                              now, ros::Duration(3.0));
    listener.lookupTransform("/turtle2", "/turtle1",
                             now, transform);
```

Funkcja `waitForTransform()` przyjmuje cztery argumenty:

1. oczekuj na transformację z układu...
2. ... do drugiego układu,
3. w podanej chwili,
4. maksymalny czas oczekiwania na transformację.

> **Uwaga:** `ros::Time::now()`używane jest tutaj tylko jako przykład i w tej sytuacji łatwiej jest pozostać przy `ros::Time(0)`. Mechanizm ten jest przydatny, gdy mamy z góry narzucony stempel czasowy, np. czas odczytu skanera laserowego.

Tak więc funkcja `waitFortransform()` zablokuje wykonywanie programu do czasu pojawienia się żądanej transformacji (zwykle kilka milisekund) lub, jeśli transformacja się nie pojawi, do upłynięcia określonego czasu oczekiwania.

Sprawdźmy, jak działa ta wersja `tf_listener`'a:

* skompilujmy kod `Ctrl+Shift+B` w VS Code
* w terminalu włączmy skrypt

  ```
  roslaunch sipr_lab1_tf turtle_tf_demo.launch
  ```

Po uruchomieniu skompilowanego kodu uzyskamy prawdopodobnie błąd:

```
[ERROR] [1287872014.408401177]: You requested a transform that is 3.009 seconds in the past, but the tf buffer only has a history of 2.688 seconds.
 When trying to transform between /turtle1 and /turtle2.
```

Tym razem będzie on jednak tylko jednorazowy i pojawi się przy uruchomieniu. Wynika on z faktu, że w momencie uruchomienia programu `tf_listener`'a program `tf_broadcaster`'a mógł nie być jeszcze uruchomiony. Takie drobne różnice w czasach uruchamiania poszczególnych węzłów nie mają wpływu na dalsze działanie systemu, ważne jest jednak aby uchronić się przed wynikającymi z nich błędami poprzez umiejscowienie zapytania `tf_listener`'a w bloku `try-catch`. Po opublikowaniu pierwszej transformacji nie powinniśmy już mieć dalszych błędów.

## TF i podróże w czasie

W poprzedniej części zapoznaliśmy się z podstawowymi możliwościami wykorzystania czasu w mechanizmie TF. Teraz pójdziemy o krok dalej i wykorzystamy najciekawsze możliwości, jakie w prosty sposób udostępnia nam TF.

Tak więc wróćmy do fragmentu kodu `tf_listener`'a, w którym wyznaczaliśmy transformację:

```c++
  try{
    ros::Time now = ros::Time::now();
    listener.waitForTransform("/turtle2", "/turtle1",
                              now, ros::Duration(1.0));
    listener.lookupTransform("/turtle2", "/turtle1", now, transform);
```

Teraz, zamiast wysyłać drugiego żółwia  w miejsce, gdzie pierwszy żółw jest teraz, wyślijmy go w miejsce, gdzie był 5 sekund temu. Zmodyfikujmy kod w następujący sposób:

```c++
  try{
    ros::Time past = ros::Time::now() - ros::Duration(5.0);
    listener.waitForTransform("/turtle2", "/turtle1",
                              past, ros::Duration(1.0));
    listener.lookupTransform("/turtle2", "/turtle1",
                             past, transform);
```

Czego spodziewamy się po uruchomieniu tego kodu? Na pewno przez pierwsze 5 sekund drugi żółw nie będzie wiedział gdzie sie poruszyć, a co potem? Sprawdźmy:

* skompilujmy kod `Ctrl+Shift+B` w VS Code
* w terminalu włączmy skrypt

  ```
  roslaunch sipr_lab1_tf turtle_tf_demo.launch
  ```

![Chaotyczny ruch żółwia 2](figures/tf_turtle2_random.png)

Jak widać żółw porusza sie w sposób nieskoordynowany, nie o to nam chodziło. Tak więc co się dzieje?

* Wysłaliśmy do TF zapytanie:

  * "Jaka była pozycja `turtle1` sprzed 5 sekund, względem `turtle2` sprzed 5 sekund?".
  
  Znaczy to, że sterowanie dla drugiego żółwia obliczamy na postawie tego, gdzie ten żółw był 5 sekund temu, oraz gdzie jego cel był 5 sekund temu.

* To o co tak na prawdę chcieliśmy zapytać to:

  * "Jaka była pozycja `turtle1` sprzed 5 sekund względem obecnej pozycji `turtle2`?".

Jak więc możemy wysłać takie zapytanie do TF? API udostępnia nam funkcje pozwalające wprost zdefiniować, w jakiej chwili ma być transformowany który układ. Kod będzie wyglądał wtedy następująco:

```c++
  try{
    ros::Time now = ros::Time::now();
    ros::Time past = now - ros::Duration(5.0);
    listener.waitForTransform("/turtle2", now,
                              "/turtle1", past,
                              "/world", ros::Duration(1.0));
    listener.lookupTransform("/turtle2", now,
                             "/turtle1", past,
                             "/world", transform);
```

Zaawansowane API funkcji `lookupTransform()` przyjmuje 6 argumentów:

1. Podaj transformacje z tego układu ...
2. w tej chwili czasu ...
3. do tego układu...
4. ... w tej chwili czasu.
5. Podaj układ, który nie zmieniał sie w czasie, w tym wypadku `world`
6. Zmienna w której przechowujemy wynik.

Zauważmy, że funkcja `waitForTransform()` również posiada API w wersji podstawowej i zaawansowanej, analogicznie do `lookupTransform()`.

![Drzewo TF – podróż w czasie](figures/time_travel.png)

Powyższy schemat pokazuje co przelicza TF. W przeszłości (czyli 5 sekund temu) przelicza on położenie `turtle1` w układzie `world`. Następnie oblicza on transformacje z układu `world` do `turtle2` w chwili obecnej. Na koniec składa te dwie transformacje.

Sprawdźmy:

* skompilujmy kod `Ctrl+Shift+B` w VS Code
* w terminalu włączmy skrypt

  ```
  roslaunch sipr_lab1_tf turtle_tf_demo.launch
  ```

Po ponownym uruchomieniu symulacji widzimy, ze drugi żółw podąża za miejscem, gdzie pierwszy był 5 sekund temu.
