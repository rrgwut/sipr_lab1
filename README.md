# SiPR - ćwiczenie 1 - Wprowadzenie do ROS

*Opracowane na podstawie:*

* **<https://wiki.ros.org/ROS/Tutorials>**
* *"Robotyka Mobilna - Laboratorium - Narzędzia wykorzystywane w programowaniu robotów mobilnych"*, Łukasz Chechliński, Daniel Koguciuk
* *"Robotyka Mobilna - Laboratorium - TF – układy współrzędnych w systemie ROS"*, Łukasz Chechliński, Daniel Koguciuk

## Wstęp

ROS (ang. Robot Operating System) jest elastycznym framework'iem do pisania oprogramowania dla robotów. Jest to zbiór narzędzi, bibliotek oraz sposobów programowania mający na celu uproszczenie zadania tworzenia złożonego i i solidnego oprogramowania działającego na różnych sprzętowych platformach robotycznych.

Dlaczego ROS jest nam w ogóle potrzebny? Ponieważ pisanie naprawdę solidnego i przenośnego oprogramowania dla robotów jest trudne. Z punktu widzenia robota problemy, które wydają się być trywialne dla ludzi wymagają do rozwiązania całego zestawu różnorodnych algorytmów. Rozwiązanie wszystkich tych problemów od zera przez pojedynczą osobę, laboratorium czy instytucję nie wydaje się być możliwe.

## Zadania do wykonania

Ćwiczenie składa się z następujących części:

0. [Podstawy Linuksa](doc/podstawy_linuksa.md)
1. [Organizacja przestrzeni roboczych ROS](doc/konsola.md)
2. [Tworzenie własnego pakietu w C++](doc/talker_listener.md)
3. [TF – układy współrzędnych w systemie ROS](doc/tf.md)
